# Docker Example

You will need to install docker on your machine for this project to run. Follow
the installation document [here](https://docs.docker.com/engine/installation/).

### Build

```bash
# build the docker image
docker build -t docker-example .
```

### Development

All source code should be added and modified inside the src directory.

To run the container in development, run the following command.

```bash
# Development
Run this project on port 8080
docker run --rm --name docker-example -d -p 8080:80 -v $(pwd)/src:/var/www/html docker-example
```

### Production

To run the container in production, run the following command.

```bash
# Production
Run this project on port 8080
docker run --name docker-example -d -p 8080:80 docker-example
```

You will need to add and configure the following code to you nginx
sites-available conf file.

```
server {
    server_name example.com;

    listen 80;

    location / {
        proxy_pass http://localhost:8080/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```
