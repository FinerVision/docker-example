FROM ubuntu:16.04

# Install basic packages
RUN DEBIAN_FRONTEND="noninteractive" apt-get clean
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y update
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y locales curl software-properties-common git
RUN DEBIAN_FRONTEND="noninteractive" locale-gen en_US.UTF-8
RUN DEBIAN_FRONTEND="noninteractive" LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php
RUN DEBIAN_FRONTEND="noninteractive" apt-get update -y

# Install PHP
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y php7.1-fpm php7.1-cli php7.1-curl php7.1-mcrypt php7.1-json php7.1-xml php7.1-mbstring php7.1-zip php7.1-mysql

# Install Nginx
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y nginx

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN DEBIAN_FRONTEND="noninteractive" apt-get install nodejs -y

# Clean-up
RUN DEBIAN_FRONTEND="noninteractive" apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# Copy PHP config files
COPY build/php.ini /etc/php/7.1/fpm/php.ini
COPY build/php.ini /etc/php/7.1/cli/php.ini
COPY build/php-fpm.conf /etc/php/7.1/fpm/php-fpm.conf

# Copy Nginx config files
COPY build/nginx.conf /etc/nginx/nginx.conf
COPY build/default /etc/nginx/sites-available/default

# Copy src to machine Nginx directory
COPY src /var/www/html

# Start PHP and Nginx services
CMD service php7.1-fpm start && nginx

# Open port 80
EXPOSE 80
